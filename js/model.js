function SinhVien(maSV, tenSV, email, matKhau,toan, ly, hoa){
    this.ma = maSV;
    this.ten = tenSV;
    this.email = email;
    this.matKhau = matKhau;
    this.toan = toan;
    this.ly = ly;
    this.hoa = hoa;
    this.tinhDTB = function(){
        return (this.toan + this.ly + this.hoa)/3;
    }
}