// Khởi tạo mảng để lưu các sinh viên
var dssv = [];

dssv = dequeueLocalStorage();
renderSinhVien(dssv);

function themSinhVien(){
    // Lấy thông tin từ form và tạo object SV
    var sv = layThongTinTuForm();
    // Thêm Sv vào mảng để lưu
    dssv.push(sv);
    // Hiện thị danh sách sinh viên
    renderSinhVien(dssv);
    // Lưu vào localStorage
    saveLocalStorage(dssv);
}

function xoaSinhVien(maSV){
    var index = dssv.findIndex(function(sv){
        return sv.ma == maSV;
    })
    dssv.splice(index, 1);
    renderSinhVien(dssv);
    saveLocalStorage(dssv);
}

function suaSinhVien(maSV){
    var index = dssv.findIndex(function(sv){
        return sv.ma == maSV;
    });
    // Show thông tin lên form
    showThongTinLenForm(dssv[index]);
    document.getElementById("txtMaSV").disabled = true;
}

function updateSinhVien(){
    var sv_update = layThongTinTuForm();
    var index = dssv.findIndex(function(sv){
        return sv.ma == sv_update.ma;
    });
    
    dssv[index] = new SinhVien(sv_update.ma, sv_update.ten, sv_update.email, sv_update.matKhau, sv_update.toan, sv_update.ly, sv_update.hoa);
    renderSinhVien(dssv);
    saveLocalStorage(dssv);
}

function resetSinhVien(){
    document.querySelector("#formQLSV").reset();
}

function searchSinhVien(){
    var nameSearch = document.querySelector("#txtSearch").value;
    var index = dssv.findIndex(function(sv){
        return sv.ten == nameSearch;
    })
    var contentHTML = "";
    for(var i=0; i<dssv.length; i++){
        var sv = dssv[i];
        if(index == i){
            var content = 
        `
            <tr class = "bg-primary">
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>${sv.tinhDTB()}</td>
                <td>
                    <button onclick = "xoaSinhVien('${sv.ma}')" class = "btn btn-danger">Xóa</button>
                    <button onclick = "suaSinhVien('${sv.ma}')" class = "btn btn-warning">Sửa</button>
                </td>
            </tr>
        `;
        contentHTML += content;
        continue;    
        }
        var content = 
        `
            <tr>
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>${sv.tinhDTB()}</td>
                <td>
                    <button onclick = "xoaSinhVien('${sv.ma}')" class = "btn btn-danger">Xóa</button>
                    <button onclick = "suaSinhVien('${sv.ma}')" class = "btn btn-warning">Sửa</button>
                </td>
            </tr>
        `;
        contentHTML += content;
    }
    document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}
