function layThongTinTuForm(){
    var maSV = document.querySelector("#txtMaSV").value;
    var tenSV = document.querySelector("#txtTenSV").value;
    var email = document.querySelector("#txtEmail").value;
    var matKhau = document.querySelector("#txtPass").value;
    var toan = document.querySelector("#txtDiemToan").value*1;
    var ly = document.querySelector("#txtDiemLy").value*1;
    var hoa = document.querySelector("#txtDiemHoa").value*1;

    // Tạo object Sinh Vien
    return new SinhVien(maSV, tenSV, email, matKhau, toan, ly, hoa);
}

function renderSinhVien(dssv){
    var contentHTML = "";
    for(var i=0; i<dssv.length; i++){
        var sv = dssv[i];
        var content = 
        `
            <tr>
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>${sv.tinhDTB()}</td>
                <td>
                    <button onclick = "xoaSinhVien('${sv.ma}')" class = "btn btn-danger">Xóa</button>
                    <button onclick = "suaSinhVien('${sv.ma}')" class = "btn btn-warning">Sửa</button>
                </td>
            </tr>
        `;
        contentHTML += content;
    }
    document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}

function saveLocalStorage(dssv){
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);
}

function dequeueLocalStorage(){
    var dataJson = localStorage.getItem("DSSV");
    if (dataJson != null){
        dssv = JSON.parse(dataJson);
        dssv = dssv.map(function(sv){
            return new SinhVien(sv.ma, sv.ten, sv.email, sv.matKhau, sv.toan, sv.ly, sv.hoa);
        })
    }
    return dssv;
}

function showThongTinLenForm(sv){
    document.querySelector("#txtMaSV").value = sv.ma;
    document.querySelector("#txtTenSV").value = sv.ten;
    document.querySelector("#txtEmail").value = sv.email;
    document.querySelector("#txtPass").value = sv.matKhau;
    document.querySelector("#txtDiemToan").value = sv.toan;
    document.querySelector("#txtDiemLy").value = sv.ly;
    document.querySelector("#txtDiemHoa").value = sv.hoa;
}